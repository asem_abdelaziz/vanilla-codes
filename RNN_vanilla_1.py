#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 01:54:54 2020

@author: asem
"""
"""
https://peterroelants.github.io/posts/rnn-implementation-part01/
https://songhuiming.github.io/pages/2017/08/20/build-recurrent-neural-network-from-scratch/
http://www.wildml.com/2015/09/recurrent-neural-networks-tutorial-part-2-implementing-a-language-model-rnn-with-python-numpy-and-theano/
https://github.com/iamtrask/Grokking-Deep-Learning/blob/master/Chapter12%20-%20Intro%20to%20Recurrence%20-%20Predicting%20the%20Next%20Word.ipynb
https://github.com/JJAlmagro/subcellular_localization/blob/master/notebook%20tutorial/FFN.ipynb
"""

import numpy as np
import itertools
import operator
from datetime import datetime
import sys


f = open('reviews.txt')
raw_reviews = f.readlines()
f.close()

f = open('labels.txt')
raw_labels = f.readlines()
f.close()

tokens = list(map(lambda x:set(x.split(" ")),raw_reviews))

vocab = set()
for sent in tokens:
    for word in sent:
        if(len(word)>0):
            vocab.add(word)
vocab = list(vocab)


word2index = {}
for i,word in enumerate(vocab):
    word2index[word]=i
    
input_dataset = list()
for sent in tokens:
    sent_indices = list()
    for word in sent:
        try:
            sent_indices.append(word2index[word])
        except:
            ""
    input_dataset.append(list(set(sent_indices)))

target_dataset = list()
for label in raw_labels:
    if label == 'positive\n':
        target_dataset.append(1)
    else:
        target_dataset.append(0)
X_train = np.load('X_train.npy')
y_train = np.load('y_train.npy')