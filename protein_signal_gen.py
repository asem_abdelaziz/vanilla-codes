#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 02:29:38 2020

@author: asem
"""

import numpy as np
import keras
import aaindex

class ProteinSignalGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self, df, aaindex_file,n_channels,
                 n_classes, shuffle=True, 
                 signal_length = None, 
                 batch_size = 1 ,
                 padding_aa = 'X'):
        'Initialization'
        self.labels = np.array(df.location)
        self.sequences = list(df.sequence)
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.aaindex = aaindex.extract_allaaindex(aaindex_file)
        self.cache = {}
        self.not_in_cache = set()
        if signal_length is not None:
            assert signal_length > 1, "signal size must be greater than 1"
            self.batch_size = batch_size
            if signal_length % 2 == 1:
                # Make even for easier padding and truncation
                signal_length -= 1
        else:
            self.batch_size = 1
            
        self.length = signal_length
        self.cache_use = 0
        self.padding_aa = padding_aa
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.labels) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        
        y = np.array(self.labels[ indexes ]).reshape((self.batch_size,1))

        y = keras.utils.to_categorical(y, num_classes=self.n_classes)
        
        # Generate data
        X = self.__data_generation(indexes)
        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.labels))
#        print("cache usage:{}/{}={}".format( self.cache_use, len(self.labels),self.cache_use/ (len(self)*self.batch_size) ))
#        print("cached:{}".format( self.not_in_cache ))
        
        self.not_in_cache = set()
        self.cache_use = 0
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __central_trunc_pad( self, sequence ):
        if len(sequence) > self.length:
            side = self.length//2
            sequence = sequence[:side]+sequence[-side:]
        else:
            lside = len(sequence)//2
            rside = len(sequence) - lside
            pad = self.length - len(sequence)
            sequence = sequence[:lside] + self.padding_aa * pad +  sequence[-rside:]
        assert len(sequence) == self.length , "len(sequence)={} != {}".format(len(sequence), self.length)
        return sequence

    def __data_generation(self, indexes):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        assert len(indexes) == self.batch_size, "indexes count must equal batch size."\
                                                "While len(indexes)={}!={}".format(len(indexes), self.batch_size)

        X = None
        signal_length = self.length
        
        if self.length is None:
            assert len(indexes) == 1 and self.batch_size ==1, "batch size must equal 1."\
                                                                "While len(indexes)={}!=1 or batch_size={}!=1".format(len(indexes), self.batch_size)
            signal_length = len(self.sequences[indexes[0]])

        X = np.empty((self.batch_size,signal_length,self.n_channels), dtype=np.float16)
        
        not_in_cache = []
        for row,idx in enumerate(indexes):
            if idx in self.cache:
                X[row,:,:] =  self.cache[idx]
                self.cache_use += 1
            else:
                not_in_cache.append(idx)
                sequence = self.sequences[idx]
                if self.length is not None:
                    sequence = self.__central_trunc_pad(sequence)
                x = aaindex.protein2signals( sequence, self.aaindex )
                X[row,:,:] = np.transpose(x)
                self.cache[idx] = X[row,:,:]

        self.not_in_cache.update(not_in_cache) 
#        print("chache use:{}/{}={}".format(cache_use,len(indexes),cache_use/len(indexes)))
        return X