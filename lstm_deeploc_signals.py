#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 03:04:08 2020

@author: asem
"""

"""
https://watermark.silverchair.com/btx431.pdf?token=AQECAHi208BE49Ooan9kkhW_Ercy7Dm3ZL_9Cf3qfKAc485ysgAAAnkwggJ1BgkqhkiG9w0BBwagggJmMIICYgIBADCCAlsGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQML_i8zp6tMXwkW39KAgEQgIICLPxqGNtyU883V8C533Khwk4aqFvNDoEryk8W_DIHE4tnQrrRrQqfsjbe06_6n1gwACEwS1QW_jOonF_7HFDoogTrc-iQaTZLJzSDngXklw2jwOwcBvhX4sJjP2WhtdUggvizUUrz_q4tnoqRmNwnQF_sojiICPB9q2bF9p6Itr71o9ACnjRaSEremMOSQ60OIcUWDM9wFEs1k1rlsl9etB2Oy2fER7mZ4KSm-o1pXXKdOqG8FqUhFYDbs_v85t9r6DwEEEnpitka3lVo5XMyKSxhTINJ30GgPfxxA-XxPHwVsdw7QzyBMoAr5tyAOfO_maqDSjW8Fgqe3N6Wv-PM-3A4hH1kNAfRBKzgz7xXzpg4z2pjrYqnSewMxlWFIKewzxqIhNK111nYdRljo2nlOFCq9uNHTNBkYJBTic4SrJ04nhgmjqSXxsFPgljwc2Z5SCYp7jr_guRfN-xcfhpvSZwdXpOu-e3o2g4u4j0MAUlOtBhZfvec8kxXwkerre4sAV26kqTCA7vRwpkL3rO1oeQmQB91Hsvvo_D2m7SHzTNaVT-n65O7EE3PD0w54YKZDjKV_XPrbe8PmZVta93EI68mYr0XzARNtfZVzVt7S3tYwVCEWtmR11inrIgdl3SslRNLnLWBBzQ-Cfr-CaS9qtZg6BbOtO0qHT-lRCpNRf2fSLSeea-JdQXFQAeQQpJq5o8LCu0MXGrxR8u8S-NSrY3JlvkN-G1L_ke-0f4
https://dl.acm.org/doi/pdf/10.5555/3305890.3306006?download=true
https://stackabuse.com/solving-sequence-problems-with-lstm-in-keras/
"""
import os
import sys
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import time
import itertools

from keras.layers import Dense, Dropout, Input, Bidirectional, GRU, SimpleRNN
from keras.layers import Conv1D, MaxPooling1D, Flatten
from keras.layers.merge import concatenate
from keras.models import Model
from keras.utils import to_categorical
from keras.optimizers import Adam
from keras.utils import plot_model
import aaindex
from protein_signal_gen import ProteinSignalGenerator

# Load the encoded protein sequences, labels and masks
# The masks are not needed for the FFN or CNN models
n_classes = 10
AAIndex = aaindex.extract_allaaindex('data/aaindex1')
n_channels = len(AAIndex)
protein_db = aaindex.deeploc_seq_loc_df('data/deeploc_data.fasta')
train_db, test_db = train_test_split(protein_db, test_size=0.2)
batch_size = 128
signal_length = 80

train_gen = ProteinSignalGenerator( train_db, 'data/aaindex1', 
                                   n_channels=n_channels,n_classes= n_classes,
                                   signal_length = signal_length, 
                                   batch_size = batch_size ,
                                   padding_aa = 'X')
test_gen = ProteinSignalGenerator( test_db, 'data/aaindex1',
                                   n_channels=n_channels,n_classes= n_classes,
                                   signal_length = signal_length, 
                                   batch_size = batch_size ,
                                   padding_aa = 'X')

# compute the number of labels
epochs = 80
n_hid = 512

n_filters = [320, 480]
maxpooling = [3, 4]
dropout = 0.3
input_shape = (signal_length, n_channels)

inputs = Input( shape = input_shape )
y = inputs
#for i in range(2):
#  y = Conv1D( filters = n_filters[i],
#              padding = 'same',
#              kernel_size = 5,
#              activation = 'relu')( y )
#  y = MaxPooling1D( pool_size = maxpooling[i])(y)
y = Bidirectional(SimpleRNN(n_hid))(y)
#y = Flatten()(y)
y = Dense( n_hid )( y )
y = Dropout(dropout)(y)
out = Dense( units = n_classes, activation = 'softmax')(y)

model = Model( inputs = inputs, outputs = out )
model.summary()
plot_model( model, show_shapes = True, to_file = 'bilstm_deeploc_signals.png')

model.compile( loss= 'categorical_crossentropy',
              optimizer = 'adam', metrics = ['accuracy'])

history= model.fit_generator( generator = train_gen,
                             validation_data = test_gen, epochs = 80 )
print(history.history['val_loss'])
%matplotlib inline

x_axis = range(epochs)
plt.figure(figsize=(8,6))
plt.plot(x_axis,history.history['loss'])
plt.plot(x_axis,history.history['val_loss'])
plt.xlabel('Epoch')
plt.ylabel('Error')
plt.legend(('Training','Validation'));


plt.figure(figsize=(8,6))
plt.plot(x_axis,history.history['accuracy'])
plt.plot(x_axis,history.history['val_accuracy'])
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.legend(('Training','Validation'));


# Plot confusion matrix 
# Code based on http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
y_pred = model.predict_generator( test_gen )
y_pred2 = np.argmax( y_pred, axis = 1 )
y_val2 = np.argmax( y_val , axis = 1 )
cm = confusion_matrix( y_val2 , y_pred2 )

plt.figure(figsize=(8,8))
cmap=plt.cm.Blues   
plt.imshow(cm, interpolation='nearest', cmap=cmap)
plt.title('Confusion matrix validation set')
plt.colorbar()
tick_marks = np.arange(num_labels)
classes = ['Nucleus','Cytoplasm','Extracellular','Mitochondrion','Cell membrane','ER',
           'Chloroplast','Golgi apparatus','Lysosome','Vacuole']

plt.xticks(tick_marks, classes, rotation=60)
plt.yticks(tick_marks, classes)

thresh = cm.max() / 2.
for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
    plt.text(j, i, cm[i, j],
             horizontalalignment="center",
             color="white" if cm[i, j] > thresh else "black")

plt.tight_layout()
plt.ylabel('True location')
plt.xlabel('Predicted location');
