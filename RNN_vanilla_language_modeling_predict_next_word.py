#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 01:54:54 2020

@author: asem
"""
"""
https://peterroelants.github.io/posts/rnn-implementation-part01/
https://songhuiming.github.io/pages/2017/08/20/build-recurrent-neural-network-from-scratch/
http://www.wildml.com/2015/09/recurrent-neural-networks-tutorial-part-2-implementing-a-language-model-rnn-with-python-numpy-and-theano/
https://github.com/iamtrask/Grokking-Deep-Learning/blob/master/Chapter12%20-%20Intro%20to%20Recurrence%20-%20Predicting%20the%20Next%20Word.ipynb
https://github.com/JJAlmagro/subcellular_localization/blob/master/notebook%20tutorial/FFN.ipynb
"""
from sklearn.model_selection import train_test_split
import numpy as np
import itertools
import operator
from datetime import datetime
import sys
from collections import Counter
from functools import reduce
from collections import defaultdict

"""
# Process data, convert text into vocab indices, compress

f = open('reviews.txt')
raw_reviews = f.readlines()
f.close()

#f = open('labels.txt')
#raw_labels = f.readlines()
#f.close()

tokens = list(map(lambda x:set(x.split(" ")),raw_reviews))
start_tkn = "SENTENCE_START"
end_tkn = "SENTENCE_END"
unknown_tkn = "UNKNOWN_VOCAB"
vocab_size = 5000

vocab = set()
counter = Counter() #sum(map( lambda sentence: Counter(filter( lambda w: len(w)>0, sentence)) , tokens ),  Counter())
for sent in tokens:
    for word in sent:
        if(len(word)>0):
            #vocab.add(word)
            counter.update([word])
#vocab = list(vocab)
vocab = list([ x[0] for x in counter.most_common(vocab_size-3)])
vocab.extend([ start_tkn, end_tkn, unknown_tkn])
word2index = {}
for i,word in enumerate(vocab):
    word2index[word]=i

X = list()
Y = list()
input_dataset = list()
for sent in tokens:
    x_sent_indices = list([word2index[start_tkn]])
    y_sent_indices = list()
    for word in sent:
        try:
            x_sent_indices.append(word2index[word])
            y_sent_indices.append(word2index[word])
        except:
            x_sent_indices.append(word2index[unknown_tkn])
            y_sent_indices.append(word2index[unknown_tkn])

    y_sent_indices.append(word2index[end_tkn])
    X.append( x_sent_indices)
    Y.append( y_sent_indices)

X_train, X_test, y_train, y_test = train_test_split( X, Y, test_size=0.2, random_state=42)
np.savez_compressed('imdb_reviews_data', 
                    X_train = X_train,
                    X_test = X_test,
                    y_train = y_train,
                    y_test = y_test,
                    word2index = word2index ,
                    vocab = vocab )
"""
data = np.load('imdb_reviews_data.npz', allow_pickle = True)
X_train= data['X_train']
X_test = data['X_test']
y_train = data['y_train']
y_test = data['y_test']
vocab = data['vocab']
word2index = data['word2index'][()]

vocab_size = len(vocab)
## initialize parameters
class RNNNumpy():
    def __init__(self, word_dim, hidden_dim = 100, bptt_truncate = 4):
        # assign instance variable
        self.word_dim = word_dim
        self.hidden_dim = hidden_dim
        self.bptt_truncate = bptt_truncate
        # random initiate the parameters
        self.U = np.random.uniform(-np.sqrt(1./word_dim), 
                                   np.sqrt(1./word_dim), 
                                   (hidden_dim, word_dim))
        self.V = np.random.uniform(-np.sqrt(1./hidden_dim), 
                                   np.sqrt(1./hidden_dim), 
                                   (word_dim, hidden_dim))
        self.W = np.random.uniform(-np.sqrt(1./hidden_dim), 
                                   np.sqrt(1./hidden_dim), 
                                   (hidden_dim, hidden_dim))

def softmax(x):
    xt = np.exp(x - np.max(x))
    return xt / np.sum(xt)

def forward_propagation(self, x):
    # total num of time steps, len of vector x
    T = len(x)
    # during forward propagation, save all hidden stages in s, S_t = U .dot x_t + W .dot s_{t-1}
    # we also need the initial state of s, which is set to 0
    # each time step is saved in one row in s，each row in s is s[t] which corresponding to an rnn internal loop time
    s = np.zeros((T+1, self.hidden_dim))
    # Asem: unnecessary line
    s[-1] = np.zeros(self.hidden_dim) 
    # output at each time step saved as o, save them for later use
    o = np.zeros((T, self.word_dim))
    for t in np.arange(T):
        # we are indexing U by x[t]. it is the same as multiplying U with a one-hot vector
#        print("U:{}, V:{}, W:{}".format(self.U.shape,self.V.shape, self.W.shape, x[t].shape))
        s[t] = np.tanh(self.U[:,x[t]] + self.W @ s[t-1])
        o[t] = softmax( self.V @ s[t])
    return [o, s]

def predict(self, x):
    o, f = self.forward_propagation(x)
    return np.argmax( o , axis = 1)


RNNNumpy.forward_propagation = forward_propagation
RNNNumpy.predict = predict

model = RNNNumpy(vocab_size)
o, s = model.forward_propagation(X_train[10])
print(o.shape)
print(o)

predictions = model.predict(X_train[10])
print(predictions.shape)
print(predictions) 



'''
the loss is defined as
L(y, o) = -\frac{1}{N} \sum_{n \in N} y_n log(o_n)
'''
def calculate_total_loss(self, x, y):
    L = 0
    # for each sentence ...
    for i in np.arange(len(y)):
        o, s = self.forward_propagation(x[i])
        # we only care about our prediction of the "correct" words
        correct_word_predictions = o[np.arange(len(y[i])), y[i]]
        # add to the loss based on how off we were
        L += -1 * np.sum(np.log(correct_word_predictions))
    return L

def calculate_loss(self, x, y):
    # divide the total loss by the number of training examples
    N = np.sum((len(y_i) for y_i in y))
    return self.calculate_total_loss(x, y)/N

RNNNumpy.calculate_total_loss = calculate_total_loss
RNNNumpy.calculate_loss = calculate_loss

print("Expected Loss for random prediction: %f" % np.log(vocab_size))
print("Actual loss: %f" % model.calculate_loss(X_train[:100], y_train[:100]))
