#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 04:12:15 2020

@author: asem
"""

import os
import sys
import numpy as np
import pandas as pd
import itertools
from Bio import SeqIO

ACESSION_NUMBER_FLAG = "H"
DATA_DESCRIPTION_FLAG = "D"
PMID_FLAG = "R"
AUTHORS_FLAG = "A"
ARTICLE_TITLE_FLAG = "T"
JOURNAL_REFERENCE_FLAG = "J"
AMINOACID_INDEX_FLAG = "I"
CORRELATED_ACESSION_NUMBERS_FLAG = "C"
BLOCK_DELIMETER = "//"
MISSING_VALUE = "NA"
AMBIGUOUS_AA = {
        'X': 'ACDEFGHIKLMNPQRSTVWY',
        'B': 'ND',
        'Z': 'EQ',
        'J': 'LI'}


def extract_content( block, flag ):
    lines = block.splitlines()
    #lines_it = iter( lines )
    for idx,line in enumerate(lines) :
        if line[:len(flag)] == flag : 
            content = " ".join(lines[idx].split()[1:])
            idx += 1
            while idx < len(lines) and lines[idx][0] == ' ':
                content += ('\n'+lines[idx]) 
                idx += 1
                
            return content
    raise NameError("flag not found in block. Flag:{}\nBlock:{}".format( flag, block ))

def extract_metadata( block ):
    try:
        meta = {}
        meta['accession_number'] = extract_content( block, ACESSION_NUMBER_FLAG );
        meta['article_title'] = extract_content( block, ARTICLE_TITLE_FLAG );
        meta['authors'] = extract_content( block, AUTHORS_FLAG );
        meta['data_description'] = extract_content( block, DATA_DESCRIPTION_FLAG );
        meta['PMID'] = extract_content( block, PMID_FLAG );
        return meta
    except NameError as error:
        print("Current block: {}".format( block ))
        print(repr(error))
        raise error
        
    
def extract_index( block ):
    index_block = extract_content( block, AMINOACID_INDEX_FLAG )
    lines = index_block.splitlines()
    tokens = lines[0].split()

    assert len(tokens) == 10, "tokens size must equal 10. {} \n {}".format( len(tokens), tokens)
    num2aa = {}
    
    for i,token in enumerate(tokens):
        aapair = token.split('/')
        num2aa[i] = aapair[0]
        num2aa[i+10] = aapair[1]

    a = lines[1].split()
    b = lines[2].split()

    aa2index = {}
    for i,token in enumerate( a + b ):
        assert i < 20, "i must be lower than 20"
        aa = num2aa[i]
        aa2index[aa] = float(token) if token != MISSING_VALUE else np.nan
        
    return aa2index

def aaindex_zscores( aa2index ):
    values = list(aa2index.values())
    mu = np.nanmean( values )
    sigma = np.nanstd( values )
    aaindex_z = {}
    for aa,index in aa2index.items():
        index = (index - mu) / sigma if not np.isnan( index ) else 0.0
        aaindex_z[aa] = index
    
    for ambig_aa, possibilities in AMBIGUOUS_AA.items(): 
        vals = map( lambda a: aaindex_z[a], possibilities )
        aaindex_z[ambig_aa] = np.mean(list(vals))
    aaindex_z['X'] = 0.0
    aaindex_z['U'] = 0.0
    aaindex_z['O'] = 0.0
    return aaindex_z
    
def extract_allaaindex( file ):
    content = ""
    with open(file, 'r') as f:
        for line in f:
            content += line
    
    accession2aaindex = {}
    blocks = content.split( BLOCK_DELIMETER )
    for i,block in enumerate(blocks[:-1]):
        try:
            meta = extract_metadata( block )
            index = extract_index( block )
            zindex = aaindex_zscores( index )
            
            data = dict({
                    'metadata': meta, 
                    'index': index,
                    'zindex': zindex})
            accession2aaindex[meta['accession_number']] = data
        except NameError as error:
            print("current block {}/{}:{}".format(i,len(blocks),block))
    return accession2aaindex


def get_deeploc_table( file = 'deeploc_data.fasta'):
    fasta_sequences = SeqIO.parse(open(file),'fasta')
    df = pd.DataFrame( columns = ['id','location','solubility','sequence_length','sequence'])
    df['id'] = df.id.astype('str')
    df['solubility'] = df.solubility.astype('str')
    df['sequence_length'] = df.sequence_length.astype('int64')
    df['sequence'] = df.sequence.astype('str')
    
    for i, entry in enumerate(fasta_sequences):
        seq = entry.seq
        description = entry.description.split()[1].split('-')
        site = description[:-1]
        solubility = description[-1]
        df.loc[i] = {
                'id': entry.id,
                'location':site, 
                'solubility':solubility, 
                'sequence_length': len(seq), 
                'sequence':seq}
    return df

def protein2signal( seq , aa2index ):
    return list(map( lambda aa: aa2index[aa] , seq ))

def protein2signals( seq, aa2indices ):
    signal = np.empty((len(aa2indices),len(seq)),dtype=np.float16)
    for i, (identifier, data) in enumerate(aa2indices.items()):
        signal[i,:] = protein2signal( seq , data['zindex'])

    return signal

def proteins2signals( proteins_table, aa2indices ):
    proteins_signals = {}
    for idx, row in proteins_table.iterrows():
        proteins_signals[ row['id']] = protein2signals( row['sequence'], aa2indices )
    
    return proteins_signals

def deeploc_seq_loc_df(file):
    proteins_db = get_deeploc_table(file) 
    proteins_db_single_loc = proteins_db[ proteins_db['location'].apply(len) == 1 ]
    locations = set( proteins_db_single_loc.location.apply(lambda l: l[0]) )
    location2idx = {}
    for idx, location in enumerate(locations):
        location2idx[location] = idx
    proteins_db_single_loc.location = proteins_db_single_loc.location.apply(lambda l: location2idx[l[0]])
    return proteins_db_single_loc

def deeploc_seq_loc(file='deeploc_data.fasta'):
    proteins_db_single_loc = deeploc_seq_loc_df(file)
    seq_x = list(proteins_db_single_loc.sequence.apply(str))
    seq_y = np.array(proteins_db_single_loc.location)
    return seq_x, seq_y

#signals = proteins2signals( proteins_db, index )

'''
np.savez('deeploc_signals', signals)
np.save('aaindex1-dict',data)
data_ret = np.load('aaindex1-dict.npy', allow_pickle=True).item()
'''

#
#for idx, row in df.iterrows():
#    print(row['c1'], row['c2'])

