#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 03:04:08 2020

@author: asem
"""

"""
https://watermark.silverchair.com/btx431.pdf?token=AQECAHi208BE49Ooan9kkhW_Ercy7Dm3ZL_9Cf3qfKAc485ysgAAAnkwggJ1BgkqhkiG9w0BBwagggJmMIICYgIBADCCAlsGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQML_i8zp6tMXwkW39KAgEQgIICLPxqGNtyU883V8C533Khwk4aqFvNDoEryk8W_DIHE4tnQrrRrQqfsjbe06_6n1gwACEwS1QW_jOonF_7HFDoogTrc-iQaTZLJzSDngXklw2jwOwcBvhX4sJjP2WhtdUggvizUUrz_q4tnoqRmNwnQF_sojiICPB9q2bF9p6Itr71o9ACnjRaSEremMOSQ60OIcUWDM9wFEs1k1rlsl9etB2Oy2fER7mZ4KSm-o1pXXKdOqG8FqUhFYDbs_v85t9r6DwEEEnpitka3lVo5XMyKSxhTINJ30GgPfxxA-XxPHwVsdw7QzyBMoAr5tyAOfO_maqDSjW8Fgqe3N6Wv-PM-3A4hH1kNAfRBKzgz7xXzpg4z2pjrYqnSewMxlWFIKewzxqIhNK111nYdRljo2nlOFCq9uNHTNBkYJBTic4SrJ04nhgmjqSXxsFPgljwc2Z5SCYp7jr_guRfN-xcfhpvSZwdXpOu-e3o2g4u4j0MAUlOtBhZfvec8kxXwkerre4sAV26kqTCA7vRwpkL3rO1oeQmQB91Hsvvo_D2m7SHzTNaVT-n65O7EE3PD0w54YKZDjKV_XPrbe8PmZVta93EI68mYr0XzARNtfZVzVt7S3tYwVCEWtmR11inrIgdl3SslRNLnLWBBzQ-Cfr-CaS9qtZg6BbOtO0qHT-lRCpNRf2fSLSeea-JdQXFQAeQQpJq5o8LCu0MXGrxR8u8S-NSrY3JlvkN-G1L_ke-0f4
https://dl.acm.org/doi/pdf/10.5555/3305890.3306006?download=true
"""
import os
import sys
import numpy as np
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import time
import itertools

from keras.layers import Dense, Dropout, Input
from keras.layers import Conv2D, MaxPooling2D, Flatten
from keras.models import Model
from keras.utils import to_categorical
from keras.optimizers import Adam

# Load the encoded protein sequences, labels and masks
# The masks are not needed for the FFN or CNN models
train = np.load('deeploc_reduced_train.npz')
X_train = train['X_train']
y_train = train['y_train']
mask_train = train['mask_train']


validation = np.load('deeploc_reduced_val.npz')
X_val = validation['X_val']
y_val = validation['y_val']
mask_val = validation['mask_val']

print(X_val.shape)
print(X_train.shape)
protein_length = X_train.shape[1]
protein_features = X_train.shape[2]
input_shape = (protein_length, protein_features,1)

# compute the number of labels
num_labels = len(np.unique(y_train))
# convert to one-hot vector
y_train = to_categorical(y_train)
y_val = to_categorical(y_val)

X_train = np.reshape( X_train, [-1, protein_length, protein_features,1])
X_val = np.reshape( X_val, [-1, protein_length, protein_features,1])


batch_size = 128
lr= 0.0025
dropout = 0.5
epochs = 80
n_filt = 10

inputs = Input( shape = input_shape )
y = Flatten()( inputs )
y = Dense(units = 30, activation = 'relu')( y )
y = Dropout(dropout)( y )
outputs = Dense( units = num_labels, activation = 'softmax')( y )

model = Model( inputs = inputs, outputs = outputs )
model.summary()
model.compile( loss= 'categorical_crossentropy',
              optimizer = Adam(lr),
              metrics = ['accuracy'])


history= model.fit( X_train, y_train,
                    validation_data = (X_val, y_val),
                    epochs = 80,
                    batch_size = 128 )
print(history.history['val_loss'])
%matplotlib inline

x_axis = range(epochs)
plt.figure(figsize=(8,6))
plt.plot(x_axis,history.history['loss'])
plt.plot(x_axis,history.history['val_loss'])
plt.xlabel('Epoch')
plt.ylabel('Error')
plt.legend(('Training','Validation'));


plt.figure(figsize=(8,6))
plt.plot(x_axis,history.history['accuracy'])
plt.plot(x_axis,history.history['val_accuracy'])
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.legend(('Training','Validation'));


# Plot confusion matrix 
# Code based on http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
y_pred = model.predict( X_val )
y_pred2 = np.argmax( y_pred, axis = 1 )
y_val2 = np.argmax( y_val , axis = 1 )
cm = confusion_matrix( y_val2 , y_pred2 )

plt.figure(figsize=(8,8))
cmap=plt.cm.Blues   
plt.imshow(cm, interpolation='nearest', cmap=cmap)
plt.title('Confusion matrix validation set')
plt.colorbar()
tick_marks = np.arange(num_labels)
classes = ['Nucleus','Cytoplasm','Extracellular','Mitochondrion','Cell membrane','ER',
           'Chloroplast','Golgi apparatus','Lysosome','Vacuole']

plt.xticks(tick_marks, classes, rotation=60)
plt.yticks(tick_marks, classes)

thresh = cm.max() / 2.
for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
    plt.text(j, i, cm[i, j],
             horizontalalignment="center",
             color="white" if cm[i, j] > thresh else "black")

plt.tight_layout()
plt.ylabel('True location')
plt.xlabel('Predicted location');
